float TwosComplement(unsigned int msb,unsigned int lsb,unsigned char Rnumber) //2's complement numbers
void UART0_TXByte(unsigned char dat);
void UART0_TXChar(unsigned char *dat);
void UART0_TX100xDat(float txdat);
void UART0_TXDat(float txdat);
void Port_Init (void);
void I2C_Start() ;
void I2C_Stop() ;
void I2C_SendACK(bit ack) ;
bit I2C_RecvACK() ;
void I2C_SendByte(uchar dat) ;
//-----------从 I2C 总线接收一个字节数据 
uchar I2C_RecvByte();
void Init_uart();
void Active_G_Sensor();
unsigned int WhoAmI();
void TempMeasure();
void ReadAccData();
void ReadMagData();
void ReadMagMaxMinData();
void Incline()	;
void eCompass();
void Uart0();
