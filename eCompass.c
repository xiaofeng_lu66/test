/**********FXOS8700CQ三轴重力加速度传感器测倾角*************
模拟I2C 测试程序
串口波特率设置为 38400
I2C: SDA=P1^1, SCL=P1^0
传感器地址：7位0x1E  SA0=0, SA1=0
UART0：TX=P0.4 RX=P0.5
T1:8位自动重载
compass:加速度Y轴正方向为0.(即正北)。传感器工作频率50Hz
************************************************************/ 
#include <compiler_defs.h>
#include <C8051F380_defs.h>
#include <stdio.h>
#include <math.h>    
#include <stdlib.h>   
#include <INTRINS.H> 

#define  SYSCLK         12000000       // System clock frequency in Hz

#define  SMB_FREQUENCY  100000          // Target SCL clock rate
                                       // This example supports between 10kHz
                                       // and 100kHz

#define SlaveAddress    0x3C//0x1E<<1 // with pins SA0=0, SA1=0 
#define WHOAMI_REG      0x0D
#define WHOAMI_VAL      0xC7
#define CTRL_REG1       0x2A 

#define ACTIVE_MODE     0x21
#define M_CTRL_REG1     0x5B
#define M_ACTIVE_MODE   0x1F
#define TEMP_REG        0x51  //[7:2-1:0]//eight 2's compliment// 0.96/LSB resolution
#define M_STANDBY_MODE//Standby模式

//Sensitivity--LSB/g
#define XYZ_DATA_CFG    0x0E//[1:0]full-scale range//Accelerometer sensor data configuration
#define FS_RANGE_2g     0x00  //Full-Scale range
#define FS_RANGE_4g     0x01
#define FS_RANGE_8g     0x10
#define SENSITIVE_2g    4096 //
#define SENSITIVE_4g    2048 //
#define SENSITIVE_8g    1024 //
//DAT REG
#define OUT_X_MSB       0x01  //[7:0]
#define M_OUT_X_MSB     0x33  //[15:8]
#define MAX_X_MSB       0x45  //
#define MIN_X_MSB       0x4B  //
//角度-弧度//弧度制转换57.32=180/3.14
#define DegToRad      0.017453292F
#define RadToDeg      57.295779F


unsigned char SMB_DATA_IN;             // Global holder for SMBus data
                                       // All receive data is written here

unsigned char SMB_DATA_OUT;            // Global holder for SMBus data
                                       // All transmit data is read from here
//-----------------------------------------------------------------------------
unsigned char g;									      	//-UART0 使用串口传入数据
unsigned int temp[16];										//I2C通讯接受数据暂存器
unsigned char widd;												//暂存器写指针
unsigned char ridd;												//暂存器读指针
unsigned char SMB_MULTIB;									//接收或发射字节数
//-----------------------------------------------------------------------------
int  Gx,Gy,Gz;													//X,Y,Z三轴重力数据
int  Bx,By,Bz;													//X,Y,Z三轴磁感数据
float  Psi, The, Phi; /* yaw, pitch, roll angles in deg */
float  VGx=0, VGy=0, VGz=0;
float  Bxcom=0, Bycom=0, Bzcom=0;								//X,Y,Z三轴硬磁校准参数
float  Bxpp=1000, Bypp=1000, Bzpp=1000;
float  tempterature;											//温度数据
float  inc_ang;														//倾角数据
//---------------------------------------
float maxBx,minBx;												//X轴磁感最大、最小值，用于软磁校准
float maxBy,minBy;												//
float maxBz,minBz;												//
//---------------------------------------

sbit SDA = P1^1;                       // SMBus on P0.0
sbit SCL = P1^0;                       // and P0.1

//-----------------------------------------------------------------------------
// Function PROTOTYPES
//-----------------------------------------------------------------------------

void PORT_Init ();
void FXOS8700_Init();
unsigned int WhoAmI();
void TempMeasure();
void ReadAccData();
void Incline();
void ReadMagData();
void MaxMinData();
void ReadMaxMin();
void eCompass();
void UART0_Init();
char putchar (char c);
void I2C_WriteReg(unsigned int RegAdd,unsigned int Command);
void I2C_Write(unsigned int RegAdd,unsigned int Command,unsigned char Multibyte);
void I2C_Read(unsigned int RegAdd,unsigned char Multibyte);
void I2C_Start();
void I2C_Stop();
void I2C_SendACK(bit ack);
bit I2C_RecvACK();
void I2C_SendByte(unsigned char dat);
unsigned char I2C_RecvByte();
void delay_ms(unsigned int z);
void Delay5us();
void Delay5ms();
void Delay50ms();

/*********************************************************
													主程序
*********************************************************/
void main()
{
	
  PCA0MD &= ~0x40;  // 关看门狗定时器
  OSCICN |= 0x03;   // 初始化内部12M系统时钟	
	EA = 1;
	
	PORT_Init();
	UART0_Init();
	printf("1 \n");
	FXOS8700_Init();
	printf("2 \n");
	while(1)
	{
		if(RI0)								//判断URAT0是否有数据输入
		{
			g = SBUF0;
			printf("OK \n");
			RI0 = 0;
		}
		
		if(g == 't')
		{
			if(WhoAmI() == WHOAMI_VAL)
			 printf("I2C_OK\n");
			else
		   printf("I2C_ERROR\n");	
			g = '\n';
		}
		if(g == 'j')
		{
			MaxMinData();
			
			g = '\n';
		}
		if(g == 'c')
		{
			TempMeasure();
			eCompass();
			g = '\n';
		}
		if(g == 'z')
		{
			ReadMaxMin();

			g = '\n';
		}

	}
}
//*********************************************************

//*********************************************************

void PORT_Init (void)
{
   P0MDOUT  = 0x00;                     // All P0 pins open-drain output

   P1MDOUT  = 0x00;                    // Make the LED (P1.3) a push-pull
                                       // output
	 P2MDOUT  = 0x00;
	
	 P0SKIP   = 0x00;
	 P1SKIP   = 0x00;
	
   XBR0     = 0x01;                        // Enable SMBus pins
   XBR1     = 0x40;                        // Enable crossbar and weak pull-ups

   P0       = 0xFF;
	 P1       = 0xFF;
 	 P2       = 0xFF;
}

/******************************************************** 
							compass初始化与计算部分
********************************************************/

void FXOS8700_Init(void)
{
 	 I2C_WriteReg(0x2A,0x20);
     I2C_WriteReg(0x5B,0x1F);
	 I2C_WriteReg(0x2A,0x21);
/*	
	 I2C_Write(0x2A,0x00,2);
	 I2C_Write(0x2A,0x20,2);
	 I2C_Write(0x2B,0x02,2);
	 I2C_Write(0x2D,0x01,2);
	 I2C_Write(0x2E,0x01,2);
	 I2C_Write(0x5B,0x1F,2);
	 I2C_Write(0x5C,0x20,2);
	 I2C_Write(0x2A,0x21,2);
*/	
	 maxBx=0;
	 minBx=0;
	 maxBy=0;
	 minBy=0;
	 maxBz=0;
	 minBz=0;
}
unsigned int WhoAmI()
{
	unsigned int whoami_val;
	I2C_Read(WHOAMI_REG,1);
	whoami_val = temp[0];
  Delay5ms();
return whoami_val;
}

//--------------------------------------------------------------
void TempMeasure()
{
	char t_lsb;
	I2C_Read(TEMP_REG,1);
	t_lsb = (char)temp[0];
	tempterature = t_lsb * 0.96;
	printf("%0.2f\n",tempterature);
}

//---加速度计-----------------------------------------------------------
void ReadAccData()
{	
	I2C_Read(OUT_X_MSB,6);
	
	Gx = (int)((temp[0] << 8) + temp[1]) / 4;
	Gy = (int)((temp[2] << 8) + temp[3]) / 4;
	Gz = (int)((temp[4] << 8) + temp[5]) / 4;
}
/*
//---倾角函数-----------------------------------------------------------
void Incline()	
{
	 float tempR; 
	 Gx -= VGx;
   Gy -= VGy;
   Gz -= VGz;
	 ReadAccData();
   tempR = Gx*Gx+Gz*Gz;
   tempR = pow(tempR,0.5);
   tempR = Gy/tempR;
   inc_ang = atan(tempR);  //倾角为Y轴在水平面的投影 
   inc_ang*=RadToDeg;//inc_ang=inc_ang*180/3.14;	
}
*/
//------磁力计--------------------------------------------------------
void ReadMagData()
{
	I2C_Read(M_OUT_X_MSB,6);
	
	Bx = (temp[0] << 8) + temp[1];
	By = (temp[2] << 8) + temp[3];
	Bz = (temp[4] << 8) + temp[5];
}
//--------------------读传感器极值寄存器------------------------------
/*
void ReadMaxMin()
{
	int X_Max, Y_Max, Z_Max, X_Min, Y_Min, Z_Min;
	I2C_Read(MAX_X_MSB,6);
	
	X_Max = (temp[0] << 8) + temp[1];
	Y_Max = (temp[2] << 8) + temp[3];
	Z_Max = (temp[4] << 8) + temp[5];
	
	I2C_Read(MIN_X_MSB,6);
	
	X_Min = (temp[0] << 8) + temp[1];
	Y_Min = (temp[2] << 8) + temp[3];
	Z_Min = (temp[4] << 8) + temp[5];
	
	printf("BX_Max=%d,BY_Max=%d,BZ_Max=%d\nBX_Min=%d,BY_Min=%d,BZ_Min=%d\n",X_Max,Y_Max,Z_Max,X_Min,Y_Min,Z_Min);
	/*
	Bxcom = (float)(X_Max + X_Min) / 2;
	Bycom = (float)(Y_Max + Y_Min) / 2;
	Bzcom = (float)(Z_Max + Z_Min) / 2;
	
	Bxpp = X_Max - X_Min;
	Bypp = Y_Max - Y_Min;
	Bzpp = Z_Max - Z_Min;
	
}
*/
//------------------采取极值数据--------------------------------------------	
void MaxMinData()
{
	unsigned int i=0;
  int maxGx,minGx,maxGy,minGy,maxGz,minGz;
	int maxBx,minBx,maxBy,minBy,maxBz,minBz;
	
	ReadAccData();
	maxGx=minGx=Gx;
	maxGy=minGy=Gy;
	maxGz=minGz=Gz;
	ReadMagData();
	maxBx=minBx=Bx;
	maxBy=minBy=By;
	maxBz=minBz=Bz;
	
	for(i=0;i<=10000;i++)
	{
	  Delay5ms(); 
		Delay5ms(); 

		ReadAccData();
	  if(Gx>maxGx)
	    maxGx=Gx;
		else if(Gx<minGx)
			minGx=Gx;
	  if(Gy>maxGy)
	    maxGy=Gy;
		else if(Gy<minGy)
			minGy=Gy;
	  if(Gz>maxGz)
	    maxGz=Gz;
		else if(Gz<minGz)
			minGz=Gz;
		
		ReadMagData();
	  if(Bx>maxBx)
	     maxBx=Bx;
		else if(Bx<minBx)
			minBx=Bx;
	  if(By>maxBy)
	     maxBy=By;
		else if(By<minBy)
			minBy=By;
	  if(Bz>maxBz)
	     maxBz=Bz;
		else if(Bz<minBz)
			minBz=Bz;
	}
	
	printf("maxGx=%d,maxGy=%d,maxGz=%d\n",maxGx,maxGy,maxGz);
	printf("minGx=%d,minGy=%d,minGz=%d\n",minGx,minGy,minGz);
	
	printf("maxBx=%d,maxBy=%d,maxBz=%d\n",maxBx,maxBy,maxBz);
	printf("minBx=%d,minBy=%d,minBz=%d\n",minBx,minBy,minBz);
	
	Bxcom = (float)(maxBx + minBx) / 2;
	Bycom = (float)(maxBy + minBy) / 2;
	Bzcom = (float)(maxBz + minBz) / 2;
	
	Bxpp = maxBx - minBx;
	Bypp = maxBy - minBy;
	Bzpp = maxBz - minBz;
	
}

//------------------------方位角计算--------------------------------------	
void eCompass()//(float Bx, float By, float Bz, float Gx, float Gy, float Gz)
{
  float sinAngle, cosAngle; // sine and cosine 
  float Bfx,Bfy,Bfz; // calibrated mag data in uT after tilt correction   
	float BX,BY,BZ;
	
	ReadAccData();				//读重力
	printf("Gx=%d,Gy=%d,Gz=%d,",Gx,Gy,Gz);
	ReadMagData();				//读磁感
	printf("Bx=%d,By=%d,Bz=%d,",Bx,By,Bz);
	//subtract off the hard iron interference computed using equation 9 
	
//------------------------------校准区域------------------------------------
//可以将重力和磁感的归零和归一处理放在这里
	//Gx -= VGx;								//重力归零
  //Gy -= VGy;
  //Gz -= VGz;

  BX = Bx - Bxcom;						//磁感归零
  BY = By - Bycom;
  BZ = Bz - Bzcom;
	
//	BX = BX * 1000 / Bxpp;		//磁感归一
//	BY = BY * 1000 / Bypp;
//	BZ = BZ * 1000 / Bzpp;
//------------------------------------------------------------------
	
// calculate roll angle Phi (-180deg, 180deg) and sin, cos 
  Phi = atan2(Gx, Gz) * RadToDeg; // Equation 2 
  sinAngle = sin(Phi * DegToRad);  // sin(Phi) 
  cosAngle = cos(Phi * DegToRad);  // cos(Phi) 
// de-rotate by roll angle Phi 
  Bfx = BX * cosAngle - BZ * sinAngle; // Equation 5 y component 
  Bfz = BX * sinAngle + BZ * cosAngle; // Bz=(By-Vy).sin(Phi)+(Bz-Vz).cos(Phi) 
  Gz = Gx * sinAngle + Gz * cosAngle; // Gz=Gy.sin(Phi)+Gz.cos(Phi) 
//calculate pitch angle Theta (-90deg, 90deg) and sin, cos
//	The = atan(-Gy / Gz) * RadToDeg;  // Equation 3//我的Keil里没有atan函数，使用这个函数结果有误，所以使用atan2
  The = atan2(-Gy,Gz) * RadToDeg;  // Equation 3        
  sinAngle = sin(The * DegToRad); // sin(Theta) 
  cosAngle = cos(The * DegToRad); // cos(Theta) 
// de-rotate by pitch angle Theta 
  Bfy = BY * cosAngle + BZ * sinAngle; //* Equation 5 x component 
  Bfz = -BY * sinAngle + BZ * cosAngle; //* Equation 5 z component 
// calculate yaw = ecompass angle psi (-180deg, 180deg) 
//  Psi = atan2(-Bfy, Bfx) * RadToDeg; // Equation 7
  Psi = atan2(Bfx, -Bfy) * RadToDeg;
	if(Psi<0)
		Psi+=360;								//Psi为方位角
	printf("Psi=%0.2f\n",Psi);
	printf("Phi=%0.2f,The=%0.2f\n",Phi,The);
}

/******************************************************** 
										串口部分
********************************************************/
//----UART0,串口初始化----//
void UART0_Init()    //比特率38400pbs
{
	SCON0 |= 0X10;
//	CKCON = 0X01;									//系统时钟/4
	CKCON |= 0X08;									//系统时钟
	TH1 = 0X64;
	TL1 = TH1;
	TMOD |= 0X20;
	TR1 = 1;
	TI0 = 1;
}
//----printf调用函数不可少----//
char putchar (char c)
{
	if (c == '\n')
		{
			while(!TI0);
			TI0 = 0;
			SBUF0 = 0X0D;
		}
	while (!TI0);
	TI0 = 0;
	return (SBUF0 = c);
}

/******************************************************** 
										I2C通讯部分
********************************************************/
void I2C_WriteReg(unsigned int RegAdd,unsigned int Command)//unsigned int SlaveAdd,
{
	  I2C_Start();
    I2C_SendByte(SlaveAddress);    
    I2C_SendByte(RegAdd);    // write register address. CTRL_REG1
		I2C_SendByte(Command);    // write data. ACTIVE mode
    I2C_Stop();
}

void I2C_Write(unsigned int RegAdd,unsigned int Command,unsigned char Multibyte)//unsigned int SlaveAdd,
{
		unsigned char i;
	  I2C_Start();
    I2C_SendByte(SlaveAddress);
    I2C_SendByte(RegAdd);    // write register address. CTRL_REG1
		for(i = 0;i < Multibyte;i++)
		{
			I2C_SendByte(Command);    // write data. ACTIVE mode
		}
    I2C_Stop();
}

void I2C_Read(unsigned int RegAdd,unsigned char Multibyte)//unsigned int SlaveAdd,
{
	unsigned char i;
	I2C_Start();
    I2C_SendByte(SlaveAddress);
	I2C_SendByte(RegAdd);
	I2C_Start();
    I2C_SendByte(SlaveAddress+1);
	for(i = 0;i < Multibyte;i++)
	{
		temp[i] = I2C_RecvByte(); 
		if(i < (Multibyte - 1))
		{
			I2C_SendACK(0);//ACK=0			
		}
		else
		{
			I2C_SendACK(1);//NACK=1				
		}
	}
  I2C_Stop();
}

void I2C_Start() 
{ 
    SDA = 1;                    //拉高数据线 
    SCL = 1;                    //拉高时钟线 
    Delay5us();                 //延时 
    SDA = 0;                    //产生下降沿 
    Delay5us();                 //延时 
    SCL = 0;                    //拉低时钟线 
} 
//-------------------I2C 停止信号 
void I2C_Stop() 
{ 
    SDA = 0;                    //拉低数据线 
    SCL = 1;                    //拉高时钟线 
    Delay5us();                 //延时 
    SDA = 1;                    //产生上升沿 
    Delay5us();                 //延时 
} 
//-------------------I2C 发送应答信号 
//入口参数:ack (0:ACK 1:NAK) 
void I2C_SendACK(bit ack) 
{ 
    SDA = ack;                  //写应答信号 
    SCL = 1;                    //拉高时钟线 
    Delay5us();                 //延时 
    SCL = 0;                    //拉低时钟线 
    Delay5us();                 //延时 
    } 
//************************************** 
//I2C 接收应答信号 
//************************************** 
bit I2C_RecvACK() 
{ 
	unsigned char  ack_nack=1;
    SCL = 1;                    //拉高时钟线 
    Delay5us();                 //延时 
    ack_nack = SDA;                   //读应答信号 
    SCL = 0;                    //拉低时钟线 
    Delay5us();                 //延时 
    return ack_nack; 
} 
//--------------向 I2C 总线发送一个字节数据 
void I2C_SendByte(unsigned char dat) 
{ 
    unsigned char i; 
    for (i=0; i<8; i++)         //8 位计数器 
    { 
        dat <<= 1;              //移出数据的最高位 -->CY
        SDA = CY;               //进位位-->送数据口 
        SCL = 1;                //拉高时钟线 
        Delay5us();             //延时 
        SCL = 0;                //拉低时钟线 
        Delay5us();             //延时 
    } 
    I2C_RecvACK(); 
} 
//-----------从 I2C 总线接收一个字节数据 
unsigned char I2C_RecvByte() 
{ 
    unsigned char i; 
    unsigned char dat = 0; 
    SDA = 1;                    //使能内部上拉,准备读取数据, 
    for (i=0; i<8; i++)         //8 位计数器 
    { 
        dat <<= 1; 
        SCL = 1;                //拉高时钟线 
        Delay5us();             //延时 
        dat |= SDA;             //读数据      
                SCL = 0;                //拉低时钟线 
        Delay5us();             //延时 
    } 
    return dat; 
} 
/******************************************************** 
										延时部分
********************************************************/
void delay_ms(unsigned int z)   //ms延时
{
	unsigned int x,y;
	for(x=z;x>0;x--)
	    for(y=1085;y>0;y--)
	;
} 
void Delay5us() 
{ 
   _nop_();_nop_();_nop_();_nop_(); 
   _nop_();_nop_();_nop_();_nop_(); 
   _nop_();_nop_();_nop_();_nop_(); 
   _nop_();_nop_();_nop_();_nop_(); 
} 
//------------------------------------
void Delay5ms() 
{ 
   unsigned int n = 560; 
   while (n--); 
}
//------------------------------------
void Delay50ms() 
{ 
   unsigned int n = 5600; 
   while (n--); 
}
